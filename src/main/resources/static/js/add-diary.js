/**
 * This file and its containing functions are used by 'add-entry.html' found in /templates.
 *
 */

const SUCCESS_ALERT = document.getElementById('success-alert')

async function submitAddDiary() {
    resetAlerts()

    // READ VALUES
    let title = document.getElementById('inputTitle').value
    let text = document.getElementById('inputText').value
    let imageURL = document.getElementById('inputImageURL').value
    let date = document.getElementById('inputDate').value
    let time = document.getElementById('inputTime').value



    var reg = /^\d+$/;

    if (!title || !text || !date || !time.substring(0,2).match(reg)
        || !time.substring(3,5).match(reg)) {
        showAlert(null, 'Invalid or empty input! Try again.')
        return
    }
    time += ':00' // Always set seconds to 00 (because of database Timestamp format)
    date += ' ' + time // Time is a part of the date in the database, so append time to date
    let data = {title: title, text: text, date: date, imageURL: imageURL}
    let response = await addDiary(data)

    const status = response.status
    let json = await response.json()
    const message = json.message

    await showAlert(status, message)
}

function showAlert(status, message) {
    let alertText
    if (status === 201) {
        alertText = document.getElementById('success-text')
        alertText.innerText = message
        document.getElementById('success-alert').hidden = false
    }
    else {
        alertText = document.getElementById('error-text')
        alertText.innerText = message
        document.getElementById('error-alert').hidden = false
    }
}

// Alerts are set to their default state 'hidden'
function resetAlerts() {
    document.getElementById('error-alert').hidden = true
    document.getElementById('success-alert').hidden = true
}