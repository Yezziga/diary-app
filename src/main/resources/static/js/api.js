const DIARIES_API = '/api/diary'


// POST REQUEST
/**
 * Create a diary with the input data
 * @param {Object} data
 * @returns {Promise<Response>}
 */
async function addDiary(data) {
    const response = await fetch(DIARIES_API, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    return response
}

// GET REQUESTS
/**
 * Fetches all diaries from database.
 * @returns {Promise<Response>}
 */
async function getDiaries() {
    const response = await fetch(DIARIES_API)

    return response
}

/**
 * Fetch a specific diary from the database based on its id.
 * @param {string} id
 * @returns {Promise<Response>}
 */
async function getDiaryById(id) {
    const response = await fetch(DIARIES_API + '/' + id)

    return response
}

// PATCH REQUEST
/**
 * Update the diary with the input data.
 * @param {object} data
 * @param {string} id
 * @returns {Promise<Response>}
 */
async function updateDiary(data, id) {
    const response = await fetch(DIARIES_API + '/' + id, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    return response
}

// DELETE REQUEST
/**
 * Sends a delete request to the API for the input id.
 * @param {string} id
 * @returns {Promise<Response>}
 */
async function deleteDiary(id) {
    const response = await fetch(DIARIES_API + '/' + id, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    })

    return response
}