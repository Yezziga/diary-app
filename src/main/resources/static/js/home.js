/**
 * This file and its containing functions are used by 'index.html' found in /static.
 *
 */

async function loadDiaries() {
    const response = await getDiaries()

    const json = await response.json()

    addDiariesToHtmlTable(json.data)
}

/**
 * Iterates all diaries and adds them to a HTML table.
 * @param {object[]} diaries
 */
function addDiariesToHtmlTable(diaries) {
    let tbody = document.getElementById('diary-table-body')

    let row = 1
    diaries.forEach(diary => {
        let tr = document.createElement('tr')

        let thRow = document.createElement('th')
        thRow.setAttribute('scope', 'row')
        let tdDate = document.createElement('td')

        // Add title as link element
        let tdTitle = document.createElement('td')
        let a = document.createElement('a')
        a.setAttribute('href', '/diary/' + diary.id)    // Set href to this diary's endpoint
        a.innerText = diary.title
        tdTitle.appendChild(a)

        thRow.innerText = '' + row++
        tdDate.innerText = diary.date

        tr.appendChild(thRow)
        tr.appendChild(tdTitle)
        tr.appendChild(tdDate)

        tbody.appendChild(tr)
    })
}