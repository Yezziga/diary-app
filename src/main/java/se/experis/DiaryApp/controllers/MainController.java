package se.experis.DiaryApp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import se.experis.DiaryApp.models.Diary;

@Controller
public class MainController {

    @GetMapping("/diary/new")
    public String newEntry(Model model) {
        model.addAttribute("diary", new Diary());
        return "add-entry";
    }

    @GetMapping("/diary/{id}")
    public String getEntry(@PathVariable("id") Integer id) {
        return "diary";
    }

}
