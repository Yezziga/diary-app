package se.experis.DiaryApp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.DiaryApp.models.CommonResponse;
import se.experis.DiaryApp.models.Diary;
import se.experis.DiaryApp.repository.DiaryRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;

@RestController("/api")
public class DiaryController {

    @Autowired
    private DiaryRepository diaryRepository;

    @GetMapping("/api/diary")
    public ResponseEntity<CommonResponse> getDiaries(HttpServletRequest request) {

        List<Diary> diaries = diaryRepository.findAll();
        Collections.sort(diaries, Collections.reverseOrder());
        CommonResponse cr = new CommonResponse();
        cr.data = diaries;
        cr.message = "Returning all diaries";

        System.out.println("Returning all diaries");

        HttpStatus response = HttpStatus.OK;

        return new ResponseEntity<>(cr, response);
    }

    @GetMapping("/api/diary/{id}")
    public ResponseEntity<CommonResponse> getDiaryById(HttpServletRequest request, @PathVariable("id") Integer id) {
        CommonResponse cr = new CommonResponse();
        HttpStatus response;

        if (diaryRepository.existsById(id)) {
            cr.data = diaryRepository.findById(id);
            cr.message = "Diary with id " + id;
            response = HttpStatus.OK;
        }
        else {
            cr.data = null;
            cr.message = "Diary not found";
            response = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<CommonResponse>(cr, response);
    }

    @PostMapping("/api/diary")
    public ResponseEntity<CommonResponse> addDiary(HttpServletRequest request, @RequestBody Diary diary) {
        CommonResponse cr = new CommonResponse();
        HttpStatus response;
        if(isValidDiary(diary)) {
            diary = diaryRepository.save(diary);

            System.out.println("New diary with id: " + diary.id);
            response = HttpStatus.CREATED;
            cr.data  = diary;
            cr.message = "Diary created";

        } else {
            cr.message = "Not valid diary inputs";
            response = HttpStatus.NO_CONTENT;
        }

        return new ResponseEntity<>(cr, response);
    }

    @DeleteMapping("/api/diary/{id}")
    public ResponseEntity<CommonResponse> deleteDiary(HttpServletRequest request, @PathVariable("id") Integer id) {
        CommonResponse cr = new CommonResponse();
        HttpStatus response;

        if (diaryRepository.existsById(id)) {
            diaryRepository.deleteById(id);
            cr.message = "Deleted diary with id: " + id;
            response = HttpStatus.OK;
        }
        else {
            cr.message = "Did not find diary with id: " + id;
            response = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(cr, response);
    }

    @PatchMapping("/api/diary/{id}")
    public ResponseEntity<CommonResponse> updateDiary(HttpServletRequest request, @RequestBody Diary newDiary, @PathVariable("id") Integer id) {
        CommonResponse cr = new CommonResponse();
        HttpStatus response;

        if (diaryRepository.existsById(id)) {
            Diary diary = diaryRepository.findById(id).get();
            if (isValidDiary(newDiary)) {
                diary.title = newDiary.title;
                diary.text = newDiary.text;
                diary.date = newDiary.date;
                diary.imageURL = newDiary.imageURL;
            }
            diaryRepository.save(diary);

            cr.data = diary;
            cr.message = "Updated diary with id: " + diary.id;
            System.out.println("Updated diary with id: " + diary.id);
            response = HttpStatus.OK;
        }
        else {
            cr.message = "Did not find diary wih id: " + id;
            System.out.println("Did not find diary wih id: " + id);
            response = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(cr, response);
    }

    /**
     * Checks if the diary contains information. A diary cannot be created if
     * it does not contain title, text or date.
     * @param diary the diary to check
     * @return true if valid, else false
     */
    private boolean isValidDiary(Diary diary) {
        return diary.date!=null && !diary.text.isEmpty() && !diary.title.isEmpty();
    }

}
