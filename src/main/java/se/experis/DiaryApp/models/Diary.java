package se.experis.DiaryApp.models;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Diary implements Comparable<Diary>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    @Column(nullable = false)
    public String title;
    @Column(nullable = false, length = 1000)
    public String text;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column
    public Timestamp date;
    @Column
    public String imageURL;
    public Integer getId() {
        return id;
    }


    // ####### GETTERS & SETTERS ########

    public void setId(Integer id) { this.id = id; }
    public String getTitle() { return title; }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }
    public Timestamp getDate() {
        return date;
    }
    public void setDate(Timestamp date) {
        this.date = date;
    }
    public String getImageURL() { return imageURL; }
    public void setImageURL(String imageURL) { this.imageURL=imageURL; }
    // ##################################

    @Override
    public int compareTo(Diary o) {
        return date.compareTo(o.getDate());
    }


}
