package se.experis.DiaryApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.DiaryApp.models.Diary;

public interface DiaryRepository extends JpaRepository<Diary, Integer> {
    Diary getById(String id);

}
